# **Week 7: Computer Controlled Machining**

This is the week where we are creating something **BIG**, and big to Neil is **life size big**.

## **Goals of the week**

### Group assignment
* Test runout, alignment, speeds, feeds, and toolpaths for your machine
* Document your work (in a group or individually)

### Individual project

* Make (design + mill + assemble) something big

### Learning outcomes

* Demonstrate 2D design development for CNC production
* Describe workflows for CNC production


____

#### My personal impression of this week:

This week, I was equally excited and fearful for the work. The thought of creating something big was thrilling, the thought of designing it however was not as exciting for me.

Our very first introduction to the marvelous ShopBot CNC machine was during the winter camp, where we briefly learned about the machine. Last week, we were re-introduced to the machine, and learned how to operate it and how take exceptional safety precautions around this particular machine.

![](pics/shopbot.jpg)

____
#### Designing

Admittedly, designing is the most challenging part of this process for me. I went through _numerous_ posts on Pinterest, Instagram and Fab academy documentations to find inspirations for the design I wanted to create.

And so with the time running and me not getting anywhere, I decided to start looking for tutorials and start designing, and perhaps if I am lucky enough the design would present itself to me!

I started by watching the [laptop stand tutorial](https://www.youtube.com/watch?v=7riGolu7BpA) to understand more about creating parametric designs and joints in fusion, all the way to [MDF box design tutorial](https://www.youtube.com/watch?v=E1JdhdI5hfU) for parametric finger joints, and [flat pack furniture tutorial](https://www.youtube.com/watch?v=DHrP1MunhFw).

While doing so, I decided to create a table with an open drawer. The design consists of 3 main parts, the legs, the "box" or drawer itself, and the curved top.

<!-- pic of the sketch -->

I wanted to try making the whole design as parametric as possible.

![](pics/parameters.jpg)

I took a top-down approach for designing the legs, starting by the base of the drawer and working down to the legs. As for the rest of the drawer, I used bottom-up approach, to create the sides and the top curved part.

and the final design looked something like this:

![](pics/object.jpg)
<iframe title="A 3D model" width="640" height="480" src="https://sketchfab.com/models/6389f4fe630a4178b056a3fe3598e38e/embed?autospin=0.2&amp;autostart=1&amp;preload=1&amp;ui_controls=1&amp;ui_infos=1&amp;ui_inspector=1&amp;ui_stop=1&amp;ui_watermark=1&amp;ui_watermark_link=1" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
        <a href="https://sketchfab.com/3d-models/side-cabnite-attempt-2-v7-6389f4fe630a4178b056a3fe3598e38e?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Side Cabnite Attempt 2 V7</a>
        by <a href="https://sketchfab.com/maha.m.alhashimi?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">maha.m.alhashimi</a>
        on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>


After the rendering process:

![](pics/rendered.jpg)

I also needed an easy way to nest all my parts on the sheet, So I followed the [Nester](https://github.com/tapnair/NESTER) [tutorial](https://www.youtube.com/watch?v=KCEwoHBlh5Y&feature=youtu.be) to install the add-in.

<!-- sheet of wood with parts -->

____
#### Milling

Before the cutting the design on the Shopbot, it is required to use the laser cutter to properly check the design for any mistakes before the actual milling.

The process of milling and assembly is halted temporarily due to the Covid-19 shutdown. I will try completing these steps as soon as access to the lab is granted again!

____
#### Assembling


____



#### My files:
