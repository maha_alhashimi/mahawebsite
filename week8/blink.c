#include <avr/io.h>
#include <util/delay.h>

int main(void){

 DDRA |= (1<<PA3);

  while(1){

    PORTA |= (1 << PA3);
    _delay_ms(1000);
    PORTA &= ~(1 << PA3);
    _delay_ms(1000);
}
}