# **WEEK 9: Input Devices**

#### Group assignment:

* Probe an input device(s)'s analog and digital signals

* Document your work (in a group or individually)

#### Individual assignment:

* Measure something: add a sensor to a microcontroller board that you have designed and read it.

#### Learning outcomes:

* Demonstrate workflows used in sensing something with input device(s) and MCU board.

____

This is the third week of distance-working on our Fab Academy assignments. Due to my limited resources at home I was not able to produce an input board for this week (I'll be designing one on eagle, produce it and program it as soon as we return), however, I did have an old Arduino Uno Kit full of sensors I can use for this week (lucky me!).

So for this week these are my personal goals (because I am quarantined, and _theoretically_ have a lot of time):

* Use a dead-board (Arduino Uno) to read an input.
* Use the atmega 328p board I produced on [Week 6](http://academany.fabcloud.io/fabacademy/2020/labs/uae/students/meha-hashmi/week6/electronics_design.html) to read an analog sensor.
* Re-Design one of Neil's input boards on eagle.
* Test the camera board for my final project!

___

### **Arduino UNO + 4X4 keypad**

The first input I am reading this week is a _Hex 4X4 keypad_ that came with the Arduino UNO kit. The hex keypad I am using this week is a simple no-name keypad with push buttons.

![](pics/keypad1.jpg)

Hex keypads typical applications range from code locks, calculators, automation systems, or simply any operation that requires a character or numeric input value.

A Hex (or a matrix) 4X4 keypad is simply an arrangement of 16 push buttons in a 4X4 matrix form. The keypad has 4 rows (R1, R2 , R3, R4) and 4 columns (C1, C2, C3, C4). At each intersecting point of the rows and columns there is a switch, thus creating a combination of 16 switches (or inputs).

![](https://www.circuitstoday.com/wp-content/uploads/2013/03/hex-keypad.png)


To detect a pressed key, the program does a method called _column scanning_:

1. Set the column pins as inputs, and the rows pins as outputs.

2. Connect the input pins to pull up resistors (alternatively use the internal pull up of the Arduino) to keep the state of the columns as HIGH.

3. The microcontroller sends `LOW` to each row at a time keeping the other rows `HIGH`.

4. The microcontroller then checks the logic status of each column on that particular row, if there is a `LOW` signal detected on the column this means that the push button is pressed.

> **Extra explanation:**   
Since the column is an input pin and is pulled up HIGH internally, no pressed button would return 1 to the microcontroller. When the button is pressed it's status now becomes 0, and both the row and the column are now connected (or short).

![](pics/column.jpg)

5. Then the same procedure is applied for the rest of the rows and the entire process is repeated.


To identify which pins are the input pins (column) and which are the output pins on the keypad, I observed the traces that connect the pin headers to each row and column.

![](pics/keypad2.jpg)

Then I connected The keypad to the Arduino Uno board and tested the code below:

```
// This code was fetched from http://www.circuitstoday.com/interfacing-hex-keypad-to-arduino and modified by Maha for fab Academy 2020.

int row[]={5, 4, 3, 2};// Defining row pins of keypad connected to Aeduino pins
int col[]={6, 7, 8, 9};//Defining column pins of keypad connected to Arduino
int i,j; // Two counter variables to count inside for loop
int col_scan; // Variable to hold value of scanned columns
void setup()
{
Serial.begin(9600);
for(i=0;i<=3;i++)
{
pinMode(row[i],OUTPUT); // sets the row pins as digital outputs
pinMode(col[i],INPUT); // sets the column pins as digital inputs
digitalWrite(col[i],HIGH);// activates the pull up resistor for the columnns.
} }
void loop()
{
for(i=0; i<=3; i++) // digital writes LOW to a row at a time.
{
digitalWrite(row[0],HIGH);
digitalWrite(row[1],HIGH);
digitalWrite(row[2],HIGH);
digitalWrite(row[3],HIGH);
digitalWrite(row[i],LOW);
for(j=0; j<=3; j++) // scans the columns in that particular row.
{
col_scan=digitalRead(col[j]);
if(col_scan==LOW) // if the column is pressed (LOW) the perform the keypress function.
{
keypress(i,j);
delay(300);
}}
}}
void keypress(int i, int j)
{
if(i==0&&j==0)
Serial.println("1"); // if the key at the first column in the first row is pressed, then it has the value of 1.
if(i==0&&j==1)
Serial.println("2");
if(i==0&&j==2)
Serial.println("3");
if(i==0&&j==3)
Serial.println("A");
if(i==1&&j==0)
Serial.println("4");
if(i==1&&j==1)
Serial.println("5");
if(i==1&&j==2)
Serial.println("6");
if(i==1&&j==3)
Serial.println("B");
if(i==2&&j==0)
Serial.println("7");
if(i==2&&j==1)
Serial.println("8");
if(i==2&&j==2)
Serial.println("9");
if(i==2&&j==3)
Serial.println("C");
if(i==3&&j==0)
Serial.println("*");
if(i==3&&j==1)
Serial.println("0");
if(i==3&&j==2)
Serial.println("#");
if(i==3&&j==3)
Serial.println("D");
}
```

Alternatively, I installed the `keypad` library by _Mark Stanley and Alexander Brevig_ by going to `sketch` tab >> `include library` >> `manage libraries`.

![](pics/library.jpg)

This library uses commands to execute the code in an easier, and much neater way.

```
/* @file CustomKeypad.pde
|| @version 1.0
|| @author Alexander Brevig
|| @contact alexanderbrevig@gmail.com
|| @description
|| | Demonstrates changing the keypad size and key values.
|| Modified by Maha for Fab Academy 2020. Changed the Pin numbers, and altered the HexKeys for experimentation.
*/
#include <Keypad.h>

const byte ROWS = 4; /* four rows */
const byte COLS = 4; /* four columns */
/* define the symbols on the buttons of the keypads */
char hexaKeys[ROWS][COLS] = {
  {'0','1','2','3'},
  {'4','5','6','7'},
  {'8','9','A','B'},
  {'C','D','E','F'}
};
byte rowPins[ROWS] = {5, 4, 3, 2}; /* connect to the row pinouts of the keypad */
byte colPins[COLS] = {6, 7, 8, 9}; /* connect to the column pinouts of the keypad */

/* initialize an instance of class NewKeypad */
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

void setup(){
Serial.begin(9600);
}

void loop(){
  char customKey = customKeypad.getKey(); // reads column and the rows of the keypad

  if (customKey){
Serial.println(customKey);
  }
}
```

Both of the codes yielded the same results.


<iframe width="560" height="315" src="https://www.youtube.com/embed/TAs5-Gdvuq4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<!--I have also tried the reading the keypad using one analog input only-->

_____

### **Atmega 328 + LDR**

The second sensor I tried this week is a light sensor. I wanted to use a photo-transistor, but unfortunately (or fortunately?) I only had an LDR available at the moment. To test this sensor, I am using the `Atmega 328` board that I created back in the [Electronics Production week](http://academany.fabcloud.io/fabacademy/2020/labs/uae/students/meha-hashmi/week4/electronics_production.html).

![](https://www.watelectronics.com/wp-content/uploads/2019/07/Light-Dependent-Resistor.jpg)

[Light dependent resistors](https://www.watelectronics.com/light-dependent-resistor-ldr-with-applications/) or photo resistors , are components with variable resistance that changes based on the light intensity that falls upon its surface. Usual applications for this type of sensor is for operations that require sensing the presence of light, for example automated street lights!

The LDR works on the principles of photo-conductivity, when light falls on the LDR, the resistance decreases, and the resistance increases back in the dark.

I used an LDR, A 100 K-ohm resistor, a breadboard, some jumper wires, an FTDI cable and of course the atmega board.

I connected one leg of the LDR to the VCC (5V) pin, and the other to the analog pin 0. The 100K resistor is also connected to the later leg, and grounded.

![](pics/LDR1.jpg)

![](pics/LDR2.jpg)

And tested a simple code to read the values of the LDR. Here we're reading the analog voltage coming out of the LDR when its connected to VCC. The higher the intensity of the light, the greater the corresponding voltage will be.

The [analog to digital converter](https://learn.sparkfun.com/tutorials/analog-to-digital-conversion/all) pin will convert the analog voltage (0 - 5V) to digital values in the range of (0-1023).

```
int LDR = A0; // sets the Analog Pin for the LDR
int Value = 0; // variable to store the value coming from the LDR

void setup() {
Serial.begin(9600); //sets serial port for communication
}
void loop() {
Value = analogRead(LDR); // read the value from the LDR
Serial.println(Value); //prints the values coming from the LDR on the serial Monitor
delay(300);
}
```

<!-- talk about the environment you are in, and how is that effecting the values, include values from the serial monitor and readings from the serial plotter and talk about them -->

I tested the LDR in normal room light and with the lights switched off. When the lights were switched off the readings went straight to `0`, and the highest reading I had in normal light was around 250. However, when I directed the flashlight of my phone on the LDR the readings went as high as 600.

I also observed the LDR readings, exposing it to flashlight when the lights were switched off, and covering the LDR when the lights were initially ON.

<iframe width="560" height="315" src="https://www.youtube.com/embed/9ZjQBnXrw2c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Then I modified the code, just so when the intensity of the light is below a certain level, the RED of the RGB I am using turns on.

```
int LDR = A2; // sets the Analog Pin for the LDR
int Value = 0; // variable to store the value coming from the LDR
int redPin = 6;// sets RED LED pin

void setup() {
Serial.begin(9600); //sets serial port for communication
pinMode(redPin,OUTPUT);// sets LED as output
}
void loop() {
Value = analogRead(LDR); // read the value from the LDR
Serial.println(Value); //prints the values coming from the LDR on the serial monitor
delay(100);

if (Value <= 100) // if the light intinsity goes below 100
{
  digitalWrite(redPin,LOW);//Turn on LED (common Anode)
}
else
{
  digitalWrite(redPin,HIGH);// Turn off LED
}
}

```

<iframe width="560" height="315" src="https://www.youtube.com/embed/Y45laemcT8s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

____
