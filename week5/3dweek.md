# **WEEK 5: 3D Scanning & Printing**


#### Group assignment:

* Test the design rules for your printer(s).

* Document your work and explain what are the limits of your printer(s) (in a group or individually)

#### Individual assignment:

* Design and 3D print an object (small, few cm3, limited by printer time) that could not be easily made subtractively

* 3D scan an object, try to prepare it for printing (and optionally print it)

#### Learning outcomes:

* Identify the advantages and limitations of 3D printing

* Apply design methods and production processes to show your understanding of 3D printing.

* Demonstrate how scanning technology can be used to digitize object(s)

___


##### This week's summary so far:

(now: Monday 2nd March 2020 12:43 am):

>I genuinely like machines .. but machines don't like me back.

(now: Tuesday 3rd March 2020 3:30 pm):

> 3D just opened a door for a whole new world to explore.

____


### Group Assignment

In our [group assignment](http://academany.fabcloud.io/fabacademy/2020/labs/uae/site/3Dprinting/3dprint.html) we tested some design rules and discovered the limitations of our 3D printers.

____


### Changing the filaments.

For most of the tasks this week, I have mainly used PLA as the filament of my choice to print my 3D designs. However, prior to printing any design, the filament required changing.

The steps to do so on the Ultimaker 3 Extended are the following:

1. Head to `materials` >> `material 1` >> `change`.
2. The print core is heated to unload the selected material.
3. The filament is then pulled back by the motor and unloaded.
4. Filament can be pulled easily to be removed from the spool holder.
5. Insert the new filament into the feeder and wait until the feeder grips it, and select the filament type.
6. Confirm that the filament is inserted to allow the motors to load the filament.
7. The Print-Core will then would be heated, and the filament should extrude from the print head.

<!-- pics of changing the filaments -->

____

#### Leveling the build plate.

calibrating the level of the build plate is significant in 3D printing as it ensures reliable adhesion of the filament to the build plate, securing the print throughout the process.

If the distance between the build plate and the print core is too large, then the print won't adhere properly. To the contrary, if the distance is too small, then the filament won't be extruded.

There are two types of leveling performed by the Ultimaker 3D printer. **Active leveling** which is automatically performed by the printer at the start of the print, and **manual leveling** in which the end user manually calibrates the distances of the build plate with the help of the instructions displayed on the screen.

In all of my prints, I used manual leveling and this is how I've done it.

1. Head to `system` > `build plate` > `manual leveling`.
2. The build plate should perform its homing procedure and the print-core would be on the center-back of the build plate.
3. Rotate and adjust all the knurled nuts clockwise until it is tightened all the way.
4. Rotate the button at the front until the plate is approximately `1 mm` from the nozzle.
5. Place the card (I used a piece of A4 paper) between the nozzle and the plate, and unscrew the nut, until a bit of resistance is felt.
6.  Click on confirm, and repeat step 5 for the left and right sides.
7. Ensure that the distances are sufficient in the second calibration by the card, and adjust if needed.

<!-- pic of calibration-->  

____


#### The X-Y offset

One of the problems I faced while I tried Printing my design for the group assignment was the wrong calibration of the Ultimaker 3D printer. The design required a support of PVA, hence adding a second filament.

In the view of my limited experience with the 3D printers, I have spent half a day trying to figure out why the print was distorted and filament won't extrude properly. The frustration blurred my mind, that I didn't think of googling the answer, and instead thought that the machine was working it's best to ruin my trials (silly.. I know!)

By the end of a long day, and with the generous help of a former Fab academy student, the solution presented was the XY offset calibration.

1. Head to `system` > `maintenance` > `calibration`> `calibrate xy offset`.
2. Wait for the 3D printer to print a grid structure on the build plate.
3. Using the XY calibration sheet, Identify the most aligned lines in the x and y grids.
4. Change values accordingly.


![](pics/xy-calibration.jpg)

____


#### The first test.

For my very first trial with 3D printing, I used a ready made [filament test cube](https://www.thingiverse.com/thing:2166102) fetched from [Thingiverse](https://www.thingiverse.com/).

I opened the `.stl` file in [Cura](https://ultimaker.com/software/ultimaker-cura) the slicer software for the Ultimaker 3D printers.

I used the basic mode to enter the following values for the purpose of testing:

* Layer height: 0.3 mm.
* Infill density: 20 %
* Infill pattern: Triangles.
* Wall thickness : 1 mm
* Wall line count: 3 walls.

![](pics/test-cube.jpg)

Then I calibrated the building plate using the previously mentioned steps, and headed for the printing set up. To start printing:

1. Place the USB into the Ultimaker 3 Extended.
2. Navigate to `print`
3. The display screen would automatically show the most recent file added to the USB, along with all the other files supported.
4. The Ultimaker would start an Active leveling, then heat the print core, extrude some filament as a test, and start printing.
5. The display screen would show the time remaining for the print to finish.

<!-- pic of steps -->

Once the print is done, I carefully removed it from the build plate and removed the extra brim layer which is a layer that extends from the edges of the design to the build plate, it is created to improve adhesion and prevent wrapping.

![](pics/test-cube-final.jpg)

The test cube purpose was to test features like small holes, bridges, small letters and overhangs. From what I have observed the lettering was clearly visible with no issues, the hole and the cylinder were printed perfectly too. However, when it came to the bigger circular and rectangular holes, the bridging on the top was slightly dented, and pieces of the filament can be observed on the inner sides.  

![](pics/test-cube-detail.jpg)

___


#### The individual Assignment [ A hinge ]

This was by far the hardest part in this week. It was not due to the machines deciding not to work, nor my lack of experience, nor the time limitations. **It was all me**. I didn't have the slightest idea of what I should be designing and printing in a way that could not be created substractivly. When ever the word design came to mind, my mind went **poof!**.

I did a lot of research, referred to many previous projects, and spent late nights on Instagram and Pinterest (working of course!) looking up models and ideas.

Finally, I gave up on my initial idea of making something extra extraordinary, and settled for a hinge. I followed a [tutorial](https://www.youtube.com/watch?v=UvY4laqJZPM) to create a hinge in Fusion 360.

The video below shows the steps of creating said hinge.

<iframe width="560" height="315" src="https://www.youtube.com/embed/HJe_yilI-kM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<!-- pic of final design-->

Then I exported the `stl` file, and opened it in Cura. The values I have selected for this design are the following:

* Layer height: 0.15 mm (normal).
* Infill density: 20 %
* Infill pattern: Triangles.
* Wall thickness : 1 mm
* Wall line count: 3 walls.

And sliced the design. The duration for this design was 3 hours and 32 minutes.

![](pics/hinge.jpg)

Then printed the design using the Ultimaker 3 extended, and a PLA filament.

![](pics/hinge-results.jpg)

The results were pretty satisfactory! The finishing was really nice! and the hinge moved easily once the brim was removed. The hinge maximum revolution angle was just a bit over 90 degrees.

What made this design exclusive to additive manufacturing was the fact that the hinge intersecting revolving parts were printed in a single process.

____

#### 3D scanning.

For the 3D scanning part, I initially wanted to use the [Sense](https://www.3dsystems.com/applications/3d-scanning). I have had several trials on it before, but the scanner probe wouldn't capture the objects I was trying to scan for some reason. I am willing to try it again in a different lightning and observe the changes.

However, due to the time limitations, I wanted to move on with the task. So I installed different applications on my phone to try scanning. Most of these apps required the usage of the front camera instead of the camera on the back (I am using an iPhone X), which made scanning slightly difficult.

Then I settled with an App called [Qlone](https://www.qlone.pro/), that required an AR mat to be placed underneath the object to be scanned. The mat with the black and white chalkboard pattern serves as a tracking marker for the software.

![](pics/ARmat.jpg)

When the object is placed on the mat and the application is at use, An AR dome is placed on the center. The dome here is an indication of the capturing process. The object scanned needs to fit into the dome. The biggest limitation here is the height of the object being scanned, but that problem can be easily solved by simply printing a bigger mat.

![](pics/scanning.jpg)
<!-- dome-->

The dome has four segments, once the scanning in a particular segment is done, then the segment turns transparent. Scanning is done by rotating around the object and turning all the segments.

<!-- scanning-->

<!--The initial result would look like this:

<!-- initial process

After processing-->

<!-- after processing-->


final result

<iframe width="560" height="315" src="https://www.youtube.com/embed/UMLBl5DH5IE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I've also tried to scan my coffee cup using the same method, however, the results were not as accurate as my previous attempt.

<iframe width="560" height="315" src="https://www.youtube.com/embed/NpOE6gKydF8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

____

#### Something extra!

One of the things I wanted to try if I had time was printing a flexible design using a rigid material. Lucky for me, we did have enough time to learn about the [Zortrax 3D printer](https://zortrax.com/) available in our Fablab.

![](pics/zortex.jpg)


I took the chance to try using the Zortrax printer to print the [flexible design](https://www.thingiverse.com/thing:826709) I acquired from Thingiverse.

To slice the design I used the [Z-suit](https://support.zortrax.com/downloads/) software.

![](pics/z-suit.jpg)

The Zortrax is mainly used for printing `ABS` materials hence the material is sensitive to the changes in the environment, and the Zortrax provides a great operation by shielding the design thus limiting any temperature variations due to external factors.

To start printing:

1. Insert the SD card with the design into the machine.
2. Navigate to `models`.
3. Select the design.
4. Select `print`.
5. The platform and the extruder will be heated respectively and then the print starts!

The Zortax displays the type of material, the amount of materials to be used and the print time.

![](pics/z-steps.jpg)

It starts by printing a raft for the print to be placed on.

![](pics/z-print.jpg)

The result:

![](pics/flexible-results.jpg)

It looks great! The outer raft can be easily broken off the main design, however, the rest of the raft on the bottom layer is very hard to remove. For other designs, that maybe acceptable, but for the design at hand, the pieces need to be separated to move properly.

I tried breaking some off using my nails and a cutter, but the parts of the design broke off and I stopped.


<!--I had another trial with this design on the Ultimaker machine using the PLA instead with no brim nor any support. And this is the result:

pic of PLA -->

____

### What I hated, What I loved, and everything in between.

Despite my rough beginning, by the end of this week, I realized that I am very astonished by the creations of a 3D printer, all the possibilities!

Yet, at the same time, I realized that 3D printers are not magical machines, there are still limitations to what can be created. At times, I was convinced that these machines had a mind of their own, refusing to cooperate in any possible way!   
