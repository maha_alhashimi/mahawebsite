# **WEEK 1: Principles and Practice**


#### Goal:

* Plan and sketch a potential final project.

#### Learning outcomes:

* Communicate an initial project proposal

____


A possible idea!  

**Function:** Language learning Assistant.  
**Aim:** Myself! And people learning languages.

## My inspiration for the idea

Growing up, I always had a passion for _learning languages_. I found satisfaction in being able to understand concepts and express my ideas in languages that weren't my native tongue.  
 I started my English learning journey early in pre-school. My Korean learning journey, however, took place in my college years (starting nearly four years ago from now) as a requirement of being the president of the Korean club at the time, and recently I have been learning Russian diligently.

During these years, I always looked for _efficient_ ways to learn languages. And I have always wished for a device that could recognize items and provide instant identification for them — consequently, the idea of creating **"the box"** [ a temporary name for easy referrals].

**"The box"** is a light weight, portable, easy to use, instant translator that can be used by kids and adults alike. It has a very minimalistic appearance and consists mainly of a camera at the back, a screen in the front, a digital speaker, a processor, a battery and some buttons for easy navigation.

**"The Box"** uses the combination of _computer vision image processing_ and _text to speech conversion_ in its leading working!

In simple words, the user selects the primary language, and the secondary language first using the buttons at the front and then runs the device. **"The Box"** identifies items in range by image recognition in the primary language, does the translation, and speaks the word while displaying it on the screen!

####  Here's a quick sketch of the operation of the Project.


![](pics/thebox.jpg)


#### And a Diagram explaining the project in details.


![](pics/theboxdiagram.jpg)
