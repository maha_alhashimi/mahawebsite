# **WEEK 4: Electronics Production**

The Fourth week of fab academy focused on electronics Production. Our goals for this week were the following:



#### Group Assignment:

* Characterize the design rules for your PCB production process

#### Individual Assignment:

* Make an in-circuit programmer by milling the PCB, program it, then optionally, trying other processes

* Document everything!

#### Learning outcomes:

* Described the process of milling, stuffing, de-bugging and programming

* Demonstrate correct workflows and identify areas for improvement if required

___

ISP [in-system programmer] is a chip or a component that allows in-circuit serial programming. Which is the ability to program devices while being installed in a complete system rather than obtaining a pre-programmed chip and installing it to the system.

This week's task was to fabricate and program the FabTinyISP with reference to the [tutorial](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html) provided.

___

### **Using the milling machine**

##### Step 1: Get the Schematics right!

The first step to fabricate this particular ISP is to obtain the traces and the board outlines for the chip from provided in the tutorial.

![](photos/png.jpg)

![](photos/tracesrml.jpg)

Both pictures shown above have `.PNG` file extensions, which is not suitable, as the software used for the milling machines only reads files with `.rml` [Red line markup language file] extension.
Therefore, the files need to converted to the correct type using an online tool called [_FAB modules_](http://fabmodules.org/). The file conversion is performed as following:

1. Select the input format to be read -- **PNG** in our case.
2. Upload the PNG image from the computer.
3. Select the output format -- because we are using a Rolland milling machine we select **Roland mill (.rml)** as our option.
4. Select the process to be done -- Here we declare whether we are processing the image of the traces or the outline of the PCB. This step is done twice.

  i. **PCB Traces (1/64)** -- refers to the traces milling bit size.

  ii. **PCB Outline (1/32)** -- refers to the outline milling bit size.

5. Enter the settings for the output format:

 * Machine: SRM-20 -- The milling machine used in FABLAB UAE.
 * X0, Y0, Z0 = 0 mm.

6. click on **calculate** [note: it would take a minute or two for the file to be produced] and then **save**.

![](photos/fabmodules.jpg)

![](photos/outputsettings.jpg)


##### Step 2: Set up the milling machine.

To fabricate the ISP chip, we are using a Roland SRM 20 machine to engrave the traces and the outlines on an FR1 [copper clad Flame Retardant 1] printed circuit board.
We are using the FR1 boards instead of the commercially found FR4 due to their safer nature, as inhaling the milling dust of an FR4 fiberglass material may lead to serious health hazards.

![](photos/rolandsrm20.jpg)

First, we mounted the FR1 plate on a medium density fiberboard (MDF) to secure the plate while milling as well to keep it levelled. The plate is adhered to the MDF using strong double sided tape where the plate is then pressed to adhere and level up properly.

![](photos/mdf.jpg)

The following steps describe the process of calibrating the machine for the milling.

1. Place the MDF board inside the Roland milling machine and secure it using the existing screws _[note: make sure to tighten diagonal screws at a time to ensure even surface]_

![](photos/securingmdf.jpg)

2. Insert the milling bit into the collet and secure it in place carefully so the milling bit doesn't fall and break the tip. we have used two different milling bits:

 a. 1/64 SE 2FL ALTIN COATED milling bit for the **traces**
 b. 1/32 SE 2FL ALTIN COATED milling bit for the **outlines**

![](photos/millingbits.jpg)

3. Using Ronald software _Vpanel for SRM-20_ on a computer connected to the machine we changed the coordinates of **X and Y** to choose a starting point (preferably at the left lower corner of the FR1 board) remembering to set the values after changing them.

![](photos/xy.jpg)

4. As for calibrating the **Z axis**, we lowered the milling bit enough for us to reach but not close enough to the board. We then un-screw the milling bit bringing its tip close to the surface to touch it slightly and then tightened it back, and set the Z value.
This step is done to ensure that the milling bit will be able to touch the surface enough to start engraving, but not be pressed into the board enough for the tip to break.


##### Step 3: Start milling!

Before going and choosing the pattern to be engraved, we can perform an experimental engrave to ensure that the milling bit tip is close enough to the surface to engrave properly.
This is done by pressing the `ON` button under `spindle` on the software page. The milling machine launches, and if the Z axis was calibrated correctly we can observe the machine engraving an indentation on the origin point and dust  starting to gather.


Once the experimental engrave is done, we can then click on the `cut` button which opens another window. Then the existing operations should be deleted and we should add our **traces** pattern file in `.rml` and finally click on `output` to launch the machine.
The milling bit is then changed again following the previous steps of `setting up the machine` and the steps in the software are repeated to engrave the outlines.

> **Important point:**
The inner pattern [traces] should always be engraved before the outer pattern [outlines]. As engraving the outer pattern first releases the chip or the piece from its place, making it difficult to engrave the inner patterns without overly moving.

![](photos/removingpiece.jpg)


##### Step 4: Soldering!

Ahead of welding, we should prepare our stations and components used. I made sure that everything was close by in my station having good source of light and good ventilation for the welding fumes.

![](photos/station.jpg)

The components I used are listed below:

Quantity | Component |
--- |---|
1 |[Atiny 45](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-2586-AVR-8-bit-Microcontroller-ATtiny25-ATtiny45-ATtiny85_Datasheet-Summary.pdf) 8 bit microcontroller.
2 |1k Ohm Resistors.
2 |499 Ohm Resistors.
2 |49 Ohm Resistors.
2 |3.3 Volts Zener Diodes.
1 |Green LED.
1 |Red LED.
1 |100 nano-Farads Capacitor.
1 |2X3 Pin Holder.

I started soldering the components on paying attention to the orientation of some components.
For example, the **LEDs** have a slight green line on one end indicating the side connecting to ground.
The **Zener diodes** also have in indication to the ground which is a fade sliver line on one end [which can be a tad difficult to see if not under a direct source of light].
The **Atiny 45** microcontroller chip has a dot that indicates the first pin and the orientation of the chip.

while soldering, I came across times where I would apply too much solder melt on a component, thus I used the de-soldering wire to remove the extra melt and prevent any damage or short circuit.

![](photos/desolder.jpg)

And finally I checked with a multimeter for any shorts between VCC and ground points, and fortunately it yielded a success! (phew!)

![](photos/multimeter.jpg)

> **Note:**
Remove the extra layer of copper on the tip of the "USB" as it may cause a short circuit.

![](photos/finalsolder.jpg)


##### Step 5: Programming

Since the platform I'm working on is Windows 10, it means that I need to setup my development environment to be able to build the program into the board. I have used the [tutorial](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/windows_avr.html) as a guide to install the toolchain into my windows system.

The programs that I have installed are the following:


1. [Atmel GNU Toolchain](https://www.microchip.com/mplab/avr-support/avr-and-arm-toolchains-c-compilers)
2. [GNU make](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/make-3.81.exe)
3. [Evrdude](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/avrdude-win-64bit.zip) (in case this doesn't work, see the update at the end of the documentation.)
4. [Zadig](https://zadig.akeo.ie/)
5. [FTS firmware ](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/fts_firmware_bdm_v1.zip)


I have downloaded each file/software tool and unzipped/placed them as instructed. Then I added the paths of GNU toolchain, GNU make and Everdude by going to the `control panel` >> `system and security` >> `system` >> `advanced system settings` >> `environment variable` >> double click on `paths` >> and add the new paths.

(or to make life simple, just search for `environment variables` in the search bar and follow up from there!)

> **EXTREMELY IMPORTANT NOTE:**
Copy the file path of your _own_ saved files instead of the paths provided in the tutorial as sometimes the paths can be outdated, or the files can have different titles.

Next, I have launched Zadig, checked for my device and re-installed the driver.

![](photos/zadig.jpg)

Then I had to check whether the programs were installed correctly. That is done by accessing git bash tool and typing `make  -v` to check for the GNU make software, `avr-gcc --version` to check for the GNU toolchain, and `avrdude -c usbtiny -p t45` to check for Evrdude software. However in my case, running the evrdude command doesn't show or do anything, this issue will later be discussed in the "problems faced" section.

![](photos/maketool1.jpg)

Next, I opened the firmware directory in gitbash and ran `run make` to create a `.hex` file.

![](photos/make.jpg)

Once the hex file was created, I carefully plugged in my USB connector and the ISP board provided by Hashim connected along a ribbon to my own ISP board and observed as the red light on both boards went on with no problems.

> To create the ribbon cable, I used a 6 color wire cable, and two 2x3 connectors. I inserted each end of the cable into a connector head, and hammered the head to secure the wires, making sure that the color order are identical at each end.

The main point of connecting the two ISPs via the ribbon is that the VCC and the GND points should be connected the same along the two ends of the ribbon as shown below:

![](photos/ribbon1.jpg)

Then I ran the commands `make flash` in gitbash. The command erases the target chip (my board) and programs its flash memory with the contents of the `.hex` file created previously.

Unfortunately, hence this step requires Avrdude to be completed, the process was forced off due to Avrdude not working on my laptop.
Thus, I have used Hashim's Laptop to perform this step along with the following few steps.

Next, I ran the command `make fuses` which sets all the fuses except the one that disables the reset pin.

![](photos/makeflash1.jpg)

![](photos/makefuses1.jpg)

To check the USB functionality, I had to plug in my own board directly and check if my computer detects the device. That is done by accessing the `device manager` and checking under `Universal Serial Bus controllers`.

The last step of programming the ISP is to blow the reset fuse by running `make rstdisbl`. This command will disable the ability to re-program the board in the future by changing the reset pin of the board to a general purpose input output pin.


To check that the ISP board is working properly as a programmer, I had a friend use it to perform the commands above to program hers and  thankfully it worked properly!

____

### **Flexible PCB!**

As an extra step, we decided to try producing a flexible ISP using the Vinyl cutter and copper sheets. Honestly, when I saw the documentations of previous students in fab academy, I thought that producing a flexible ISP would be much easier, and less time consuming than using the milling machine.

Well, I was wrong. We spent a good amount of time trying to find out the best cutting settings that would produce a sustainable chip. Everyone pitched in with ideas for our trials and errors and the process turned into a group work which can be found in our [Group Assignment page](http://academany.fabcloud.io/fabacademy/2020/labs/uae/site/eproduction/epro.html).

| A summery of the production process that worked best for us:|
|----|
1. Adhere the copper sheet to a piece of double sided tape, and then place the other side of the tape on a transparent transfer paper.
2. Insert the sheet into the vinyl cutter.
3. Choose `Piece` so the vinyl cutter can take the dimensions of the sheet.
4. Set the force between `140 % ~ 150 %`.
5. Import the board image in **JPG** directly to **Roland cut studio** (refer to [this tutorial](https://www.youtube.com/watch?v=mQjLP4vibVA))
6. Check the image dimensions to make sure that the pads and traces will fit the components. Refer to the image dimensions in fab-modules for comparison.
7. Use the bitmap tool to change the image from raster to vector.
8. Perform the cutting process by pressing on the `cutting` icon on the top tool bar.


The process of weeding and soldering the components were done individually, thus is documented in details below.

This is the chip I produced for using the vinyl machine. It is noticed that it's looks much better than its decedents, as the traces and pads are clear and wide enough for soldering.

![](photos/image1.jpg)

The next step was the weeding, where I carefully pulled the excess copper around the pads and the traces. **A useful tip** is to use a good pier of tweezers to remove the large parts, x-acto knife to release any parts that might get stuck, and some small scissors to cut large pieces as you weed away the copper.  

![](photos/image2.jpg)

Next, I applied some double-sided tape to the back of the tracing paper and adhered it to a piece of acrylic I've cut previously using the milling machine. And finally removed the excess tape and excess copper on the sides.

![](photos/image3.jpg)

Next was the soldering. Now this step was a bit more difficult to compared to the chip produced using the milling machine.

When soldering, the traces or the pads (sometimes both!) would lift off the tracing paper, the soldering bits would not stick to the copper so I was extra careful and patient in soldering the components. Also, settings the temperature of the soldering iron too high caused the tracing paper underneath to melt so I kept the temperature at `350` which was ideal for my soldering.   

![](photos/image6.jpg)

Then it was the time for programming. I used the previously mentioned steps to program my ISP. However, when I tried plugging the ribbon off, the traces came out with it :( and I had to apply some solder on it to fix it again!

![](photos/image7.jpg)

____

### **Producing an ISP using the fiber laser machine**

This step (like many others!) was full of successes, failures, frustrations and proud satisfaction.

We tested our Trotec fiber laser machine to characterize it's cutting settings before producing our own chips. All the details for the process along with the results are documented in the [Group Assignment page](http://academany.fabcloud.io/fabacademy/2020/labs/uae/site/eproduction/epro.html).

To produce my own chip, I worked with my fellow student and friend Fatima to test the settings on for the ISP chip. We have used [this page](https://gitlab.fabcloud.org/academany/fabacademy/2020/bootcamp/spicy/blob/master/PCB_on_fiber.md) as a reference.

For our first trial, we replaced the `C02` lens in our trotec machine with the `fiber laser` lens.

![](photos/lens.jpg)

Then we uploaded the traces on `CoralDraw` keeping the color as it is (black) for the purpose of engraving, and clicked on print. The detailed process of using the Trotec laser machine can be found in [Week 4]() page.

**Attempt 1: The start**

The settings that we used for this test are the following:

* Power: 60
* Speed: 10
* PPI: 30000 HZ
* Passes: 4
* Correction: 10
* Under custom: Check _High Quality_ and _Correction_
* Position: Top right of the bed.

And the result was this:

![](photos/attempt1.jpg)

The traces and the pads of the PCB were clearly visible and well marked. However, a good amount of copper remained on the surface of chip causing short circuits to almost every single connection when tested using the multi-meter. **Verdict: Absolute fail**.

**Attempt 2: The success!**

For the second attempt, we kept all the settings used previously, and increased the **passes** to **10** as it proved a good result in our group assignment test.

And the results?

![](photos/attempt_perfection.jpg)

They were amazing! Traces and pads fully outlined perfectly, not a single short circuit when tested using the Multimeter. **Verdict: SUCCESS!!!!**.

**Attempts 3 + 4: The disappointment !**

Hence the previous settings worked perfectly for us, we decided to engrave another board (since we worked together to produce the chip).  And well, this is how I felt.

![](https://media.giphy.com/media/xKgpQqi9rs5Ms/giphy.gif)

We engraved the boards using the **EXACT SAME SETTINGS**, but we missed the fact that the FR1 plate was **not levelled properly** on the MDF board, thus resulting in improper removal of the copper and short circuits. **Verdict: Disappointing fail**

![](photos/attempt3.jpg)



**Attempt 5: The cut trial**

The FR1 plate had to be changed, since there wasn't enough space for another engrave. But before that we decided to try cutting our successful PCB attempt.

We changed the lens back to the CO2 lens., and we've used the cutting settings provided in the tutorial. And it _almost_ worked.

![](photos/attempt5.jpg)

The outlines were very charred, and only one side of the outlines was properly cut hence the reference point was lost.

> **A TIP :**
 Try not to move the placement of the engraving job on the software, or else the outlines would be offset-ed and you'll have to manually guess the reference point .. which proved to be a bad idea.


 **Verdict: fail**.

**Attempt 6: Another trial**

We came back to the Trotec machine after a very much needed break. And decided to engrave a several ISP chips again. We used the _successful engrave settings_ and it yielded perfect results! The process of took about 1 hour and 27 minutes.


Then we replaced the fiber lens with the CO2 lens (again) to attempt the cutting again. And used the previous cut settings. The outlines were not cut! We tried different settings, varying the power, speed and the passes every time but the outlines just wont cut!!


So after thoroughly looking at the causes of the error, we found out that the lens was not installed properly, thus the focus was wrong. **After** fixing the lens and trying again, several times, it still didn't cut the traces.

![](https://media.giphy.com/media/sUo3Ud27QWHfy/giphy.gif)

This is what it looked like after all the trials:

![](photos/laserfinal.jpg)

We decided that by this point, we had two options. Either we can take the risk of cutting the outlines using the milling machine, trying to use our luck to manually locate the reference point and cut it. Or, we could use a saw to cut the outlines.

So I went with the easier one (in my opinion!), the milling machine option. I tried to locate the reference point by eye and I started cutting. The results were considered pretty good.

![](photos/laser_individual.jpg)

____

### Problems Faced (mostly in programming)

> Because what's life without a set of challenges!

1. _Outdated program installation links_

  Some of the software required installation in windows had outdated links in the tutorial guide which lead us to manually search for the software and choose the version that best suited our computer systems (links for the updated programs are mentioned above under programming.)

2. _Outdated paths_

Much like the outdated installation links, the paths in the tutorial were outdated and referred to older versions of the software. Thus copying the exact path from the tutorial rendered a fail in the system recognizing the software. The solution to this is to copy the file path directly of the downloaded software files.

3. _The solder jumper_

The solder jumper! Something that I completely missed on while soldering my components! When I plugged on my ISP chip to the ribbon and the programming ISP borrowed from Hashim, I noticed that the red light was not turning on and the commands I ran rendered no results! Soon I discovered that the jumper solder was not even soldered! thus not completing the circuit and not providing power to the rest of the components. The only solution to this was to go back and solder the part, so I did just that!

4. _The Evrdude problem_

One of the main tools required to set up my ISP was not working as it should be. After re-installing the files, and checking the paths for any errors, I tried running the command to recognize the tool on Gitbash, but the results shown were nothing, not confirmation of the existence of the tool nor an error message.

Assuming that I can skip this step, I moved forward to the next few commands and was stopped again by an error message when I tried running the `make flash` command which required the usage of Evrdude tool.

![](photos/everdude.jpg)

The problem is apparently exists for all my colleagues with a windows 10 x64 bit systems installed. The problem main cause and solution is yet to be known.

____

### **Update March 15, 2020**

I tried re-installing the AVR Gnu tool-chain using [Hala's](http://fabacademy.org/2020/labs/barcelona/students/hala-amer/weeks/w05.html) wonderful and very useful documentation to solve my AVR-dude problem.

The steps that I followed:

1. Install [WinAVR 20100110](https://sourceforge.net/projects/winavr/) and launch it.

2. Open the WinAVR folder and find the folder bin. Then delete or move these two files elsewhere: **avrdude.conf** and **avrdude**.

3. Replace the deleted folders with the newer version of them [Version 6.3 2016](http://download.savannah.gnu.org/releases/avrdude/).

4. In `control panel` > `systems` > `advanced system settings` > `environmental variables` > `path` > `edit` > delete the old AVR (if installed) path and add the new path (../WinAvr/bin).

5. Connect the ISP programmer to the laptop, and launch Zadig, select the programming > select "libsub-win32 (v1.2.6.0)" > replace driver.

6. Open the fts_firmware_bdm_v1 folder and open GitBash.

7. Make sure that all the driver are installed properly:

> Make -v

![](photos/make2.jpg)

>Avr-gcc --version

![](photos/Avr2.jpg)

>avrdude -c usbtiny -p t45

This time the AVRdude was installed properly as shown.

![](photos/avrdude2.jpg)

8. Follow the steps of programming the ISP by following the steps mentioned earlier in the documentation.

>make flash  
>make fuses   
>make rstdisbl
