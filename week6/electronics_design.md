# **WEEK 6: Electronics Design**

### Goals this week:

#### Group assignment:

1. Use the test equipment in your lab to observe the operation of a microcontroller circuit board (in minimum, check operating voltage on the board with multimeter or voltmeter and use oscilloscope to check noise of operating voltage and interpret a data signal)

2. document your work (in a group or individually)

#### Individual assignment:

1. Redraw one of the echo hello-world boards or something equivalent, add (at least) a button and LED (with current-limiting resistor) or equivalent input and output, check the design rules, make it, test it.

#### My goals this week:

1. Re-draw the Atmega 328p board, add at least an LED, a button and an extra component, make it, check the design rule and test it if possible.
2. Try different software for the designing, A few to try: Eagle, kicad, circuit maker and easyEDA.
3. Mods? fab modules or flat cam? test and record the difference.
4. Try a software to simulate your circuit.

#### Learning outcomes

1. Select and use software for circuit board design

2. Demonstrate workflows used in circuit board design


____

### **Hello Board Attiny 44**


For this task I used the [fab academy reference](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo) to create the Atiny44 circuit board.   

Since we used eagle on the previous winter camp, it was my preferred tool to create the circuit. After [installing](https://www.autodesk.com/products/eagle/free-download) eagle, I also had to install the [Eagle fab library](https://gitlab.cba.mit.edu/pub/libraries) to ensure that I was using components with modified sizes that relay maximum efficiency when designing the board.

To start using the fab library, I had to physically copy the file into the eagle lib directory.

![](images/movinglibrary.jpg)

Then I created a new project in eagle and started working in the schematic window. I clicked on the `add part` icon on the left hand tool bar to access the library and navigate to the fab library named `eagle_fab` in my laptop.

![](images/eaglefab.jpg)

Then I looked for each component, and added them to my schematic. Since we were required to add at least one input and one output to our circuit, I decided to add an RGB LED along with three resistors one for each LED along with a button. The components I used are listed below:

Quantity | Component |
--- |---|
1 |[Atiny 44](http://ww1.microchip.com/downloads/en/DeviceDoc/8006S.pdf) 14 pin microcontroller.
1 |XTAL Resonator.
1 |10 Kilo Ohm resistor.
1 |1 micro-farads Capacitor.
1 |3x2 AVRISP SMD connector.
1 |6 pin header.
1 |RGB_new _fancy_ fab.
3 |499 Ohm Resistors.
1 |6 MM Switch.

I placed the components just as Our instructor Hashim taught us with the connector having its VCC side on the _right_ and the GND on the _Left_ and used tags to connect the components together instead of using direct links, hence it would be easier to modify later, and much more comfortable on the eye.

![](images/schematicfinal.jpg)

After ensuring that the components are connected directly, I converted the schematic diagram to a Board diagram using the `switch/generate board` tool on the top tool bar.

![](images/board.jpg)

I moved the components to the workspace first , and then clicked on the `DRC` icon on the left tool bar. This tool is the design rule check, and it checks the design and routing based on the values fed in by the user.

This step is crucial to ensure that we are applying enough constraint on the design so the milling bit would engrave the traces and pads of the components correctly ensuring that the design would function properly and reliably.

 In this window, I changed all the values under `clearance` to `17 mil` and changed the `minimum width` under `Size` to `17 mil` as well.

![](images/clearance.jpg)

 **THEN** came the hard part!  
This is where I spent _most_ of my time on this task clenching my fists in exasperation!

I used the routing tool on the left toolbar to connect the traces between the components. Most of the components were easy to connect, however, when it came to the ground and VCC, some connections kept clashing or crossing.  

![](images/routingtool.jpg)

I changed the placement of the components, changed the tags in schematic and redid the routing, tried squinting and looking hard at the board to look for a solution (_which needless to say, didn't work at all_).

Eventually, it got quite frustrating that I kept postponing this task .. _not the best idea =(_


After a long time of trials and errors, and contemplating the right answer, I figured out that I should just simplify this and use a single LED with a single additional 499 Ohm resistor.  
Then later after consulting Hashim who told me that I _can_ change the `wire width` to 12 instead, I was finally able to connect all the components together.

I re-clicked on the `DRC` icon to check for any errors, and man I had some! I had an air-wire hanging and some wire stubs. Fixed that quickly by re-routing and removing the stubs and were good to go!

![](images/errors.jpg)

I also opted for changing the resolution by going to the `Grid` icon in the upper left tool bar and change the default value `50` to `10` which permitted smoother routing.

![](images/grid.jpg)


Before exporting my image, I had to hide all the layers except the red layer that shows my connections, that is done by clicking on the `layers` icon on the upper toolbar, hiding all the layers and showing only the red.

![](images/layers.jpg)

Now that leaves me with the labels that I also want to hide by going to `options` tab >> `set` >> and `MISC` >> de-check everything that has `display`

![](images/labels.jpg)

Finally my board diagram looked like this!

![](images/finalboard.jpg)

To export my board as an image by going to `file` >> `export` >> `image`, and change the image characteristics. I chose the maximum resolution value `2000 dpi` and checking the `monochrome` box.

![](images/exportimage.jpg)

The pictures are then saved in the directory of my choice in `PNG` format.  
I used paint editor to edit the traces image and create the `outlines` by simply Covering the traces with a white filled BOX.

![](images/traces.jpg)

Then I used the [fab modules](http://fabmodules.org/) to create the `.rml` format for my traces and outlines. The exact steps for the process can be found on my [electronics production](week2/electronics_production.html) page.

while performing this step, I found out that the parameters of the picture was suspiciously too large, in fact it was double the size of the board I had on eagle!   
This was due to the fact that I had my laptop display settings to `150 %` when exporting my image, which resulted in deformed parameters.

![](images/rml.jpg)

Then I used the milling machine to engrave my traces, and I soldered the components. The exact steps to do so are in the [electronics production](week2/electronics_production.html) page.

![](images/finalchip.jpg)

____

### **Atmega hello.328 Board**

My extra work this week was to redesign a basic `hello.328p` board, I used the board Niel [created](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo) and added inputs and outputs that would resemble the operation of my final project board.

For this task, I also used Eagle, hence it is the software that I am most confident with.

The list of components I used on this board are the following:

Quantity | Component |
--- |---|
1 |[Atmega 328P/88A](https://www.microchip.com/wwwproducts/en/ATmega328p) 8-bit AVR micro-controller.
1 |16 MHz Crystal Resonator.
1 |10 Kilo Ohm resistor.
2 |SMD push buttons.
1 |100 PF capacitor.
2 |22 (or 18) PF capacitors.
1 |0.1 uF capacitor.
2 |10 uF capacitors.
1 |100 uF capacitor.
1 |10 K-Ohm resistor
1 |0 Ohm resistor.
2 |1 K-Ohm resistor.
1 |499 Ohm resistor.
1 |RGB LED.
1 |Mosfet transistor type N.
1 |Diode.
1 |FTDI connector (1x6)
1 |ISP connector (2x3)
3 |Through hole connector (1x4)
1 |Terminal (1x2)
1 |Power jug connector (2x2)
1 |Speaker.
1 |OLED screen.

For this board, I tried using outputs that I might use in my final project. To understand the connections of the board, I used the [input devices](http://academy.cba.mit.edu/classes/input_devices/index.html) and [output devices](http://academy.cba.mit.edu/classes/output_devices/index.html) week pages references.

The process of creating this board took a lot of time. The mere process of thinking and researching about the modules that could be added was time consuming, but it was definitely worth it as I learned a lot.

Hence, my schematic:

![](images/schematic-atmega.jpg)

As usual, routing took the best chunk of time. This time instead of connecting all the `GND` points, I created a "common ground" by selecting the `polygon` on the left tool bar, selecting the area around my components and clicking on `Ratsnets`.

![](images/common-ground.jpg)

My final board diagram:

![](images/atmega-board.jpg)

This time **instead** of using fab modules, or mods, I used a software called [FlatCam](http://flatcam.org/) which is an open source program that prepares CNC jobs to create a PCB on a CNC router. It basically takes the Gerber files generated by the PCB cad program and creates a G-code for the isolation routing.

The reason why I am currently preferring FlatCam over other tools available is the ease of transition and conversion of the files; a simple modification on eagle for example would be followed be several other steps when using the other tools, but the transition is quite easy with flatcam as I only need to export the files once only.

To export to flatcam, I use the `manufacturing` tool on the right side of the board diagram page, and `launch cam` to open the cam processor, generate and export the Gerber files.

![](images/manufacturing.jpg)

Then I lauch flatcam and open the `Gerber files` (for the traces and the outlines) and the `Excellon files` (for the drill parts) required, and enter the values accordingly.

![](images/gerber_files.jpg)

![](images/traces-flatcam.jpg)

![](images/outlines-flatcam.jpg)

![](images/drill.jpg)

I used the Roland SRM-20 CNC machine for milling the board, but modified the `setup` in the software to accommodate the file extension I am using.

![](images/milling.jpg)

I initiated the process by milling the traces, and at the very first trial something was not quite right.

![](images/no-gnd.jpg)

The pads representing the ground were not engraved. After discussing this with Hashim, it turns out that I forgot to change the settings for the `thermal Isolation` in `DRC` which basically substracts the pads from the signal polygon, in my case, the common ground.

![](images/thermal-isolation.jpg)

Now processing the files again for the milling would have been a long process again, but I am using FlatCam, So I only needed to regenerate the Gerber files again! Ha!

Trying the milling again, and the `GND` pads were engraved this time. I also drilled the holes for the connectors.

![](images/with-gnd.jpg)

And finally the outlines, hence the final board:

![](images/final-mill.jpg)

Then came the soldering stage, which admittedly, wasn't quite easy either. But hey! its finishing the hard tasks that brings the most satisfaction!

One of the reasons why soldering took a good chunk of time this week, is the placement of some components, for example, my FTDI traces were right in the middle! which is a big mistake. Thus, I turned it into a through hole connection and drilled the holes myself using the manual drill.

![](images/ftdi.jpg)

Another difficulty I faced in soldering this board, was the `Atmega 328` chip itself, the pins were very small and hard to solder one at a time. Thus I used a technique where I spread solder on the pins and removed the excess with the de-soldering pump.

And finally, this is how my board looked like.

(The red wire connected my reset resistor to the reset pin of the atmega, the trace was cut when I tried drilling the holes of the FTDI).

![](images/final.jpg)
